<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tempname
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<?php
		get_sidebar( 'footer' );
		
		if ( is_active_sidebar( 'footer-5' ) ) { ?>
		<div class="legal-info">
			<?php dynamic_sidebar( 'footer-5' ); ?>
		</div>
		<?php } ?>

		<div class="site-info">
			<?php printf( esc_html__( 'Made with %1$s', 'tempname' ), '<i class="fa fa-heart" aria-hidden="true" aria-label="love"></i>
' ); ?>
			<span class="sep"> | </span>
			<?php echo '&copy; ' . date('Y') . ' '; bloginfo( 'name' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
