<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tempname
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<div class="entry-meta-inside">
			<?php
				// tempname_posted_on();
				// tempname_posted_by();
				$categories_list = get_the_category_list( esc_html__( ', ', 'tempname' ) );
				if ( $categories_list && tempname_categorized_blog() ) {
					printf( '<span class="cat-links">' . esc_html__( 'in %1$s', 'tempname' ) . '</span>', $categories_list ); // WPCS: XSS OK.
				}
			?>
			</div>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php //tempname_post_thumbnail(); ?>

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php tempname_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
