<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Twirly_Whiskers
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
<header class="entry-header" <?php if ( has_post_thumbnail() ) { ?>style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)" <?php } ?>>
<?php if (! is_page_template('page-templates/landing-page.php') && ! is_front_page()) { ?>
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
<?php }
	elseif ( is_front_page() ) {
		if ( is_active_sidebar( 'hero-area' ) ) {
			dynamic_sidebar( 'hero-area' );
		} 
	}
?>
	</header><!-- .entry-header -->
	

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'twirlywhiskers' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'twirlywhiskers' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
