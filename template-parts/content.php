<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tempname
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" <?php if ( has_post_thumbnail() ) { ?>style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)" <?php } ?>>
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else : ?>
			
			<a href="<?php echo esc_url( get_permalink() ); ?>" class="entry-link" rel="bookmark">
				<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
			</a>
		<?php
		endif;

		if ( 'post' === get_post_type() ) :	?>
		<div class="entry-meta">
			<div class="entry-meta-inside">
			<?php
				// tempname_posted_on();
				// tempname_posted_by();
				$categories_list = get_the_category_list( esc_html__( ', ', 'tempname' ) );
				if ( $categories_list && tempname_categorized_blog() ) {
					printf( '<span class="cat-links">' . esc_html__( 'in %1$s', 'tempname' ) . '</span>', $categories_list ); // WPCS: XSS OK.
				}
			?>
			</div>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php //tempname_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		if ( is_singular() ) {
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'tempname' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );
		} else {
			the_excerpt();
		}

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tempname' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php tempname_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
