<?php
/**
 * Template part for displaying video posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Twirly_Whiskers
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class="entry-header" <?php if ( has_post_thumbnail() ) { ?>style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)" <?php } ?>>
		<?php
			if ( is_singular() ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
			} else { ?>
			<a href="<?php echo esc_url( get_permalink() ); ?>" class="entry-link" rel="bookmark">
					<?php
					the_title( '<h2 class="entry-title">', '</h2>' );
					?>
			</a>
			<?php } ?>
<div class="video-container">
<?php
the_content();
?>
</div>
<?php
		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<div class="entry-meta-inside">

				<!-- <span>by <?php //the_author_posts_link(); ?></span> -->

				<?php
				$categories_list = get_the_category_list( esc_html__( ', ', 'tempname' ) );
				if ( $categories_list && tempname_categorized_blog() ) {
					printf( '<span class="cat-links">' . esc_html__( 'in %1$s', 'tempname' ) . '</span>', $categories_list ); // WPCS: XSS OK.
				}
				?>
			</div>
		</div> <!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		if ( is_singular() ) { 
			// the_title( '<h1 class="entry-title">', '</h1>' );
		?>
		<div class="post-details">
		<!-- <p>This is a video post!</p> -->
		<?php

			if ( has_post_format( 'video' ) ) {
				// echo 'this is the video format';
				$duration = get_post_meta( get_the_id(), 'lesson_duration', true ); 
				if($duration){
					echo '<p><b>'.  __( 'Duration', 'my-library' ) .':</b> ' . $duration . '</p>';
				}
			} else {
				echo '<p><b>Estimated reading time:</b> ' . estimate_reading_time() . '</p>';
			}
		?>
		</div>
		<?php
			//the_content( sprintf(
				/* translators: %s: Name of current post. */
				//wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'tempname' ), array( 'span' => array( 'class' => array() ) ) ),
				//the_title( '<span class="screen-reader-text">"', '"</span>', false )
			//) );
		
		} else {
			echo '<div class="view-full-post"><a href="' . esc_url( get_permalink() ) . '" class="more-link button secondary" rel="bookmark">'.  __( 'Watch', 'tempname' );
			if($duration) {
				echo ' ' . __( 'in', 'tempname' ) . ' ' . $duration;
			}
			echo '</a></div>';
			// the_title( '<h2 class="entry-title">', '</h2>' );
			
			// the_excerpt();
		}

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tempname' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php tempname_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
