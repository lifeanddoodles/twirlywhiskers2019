<?php
/**
 * Template part for displaying quote posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tempname
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class="entry-header" <?php if ( has_post_thumbnail() ) { ?>style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)" <?php } ?>>
	<?php
	if ( is_singular() ) {
		the_title( '<h1 class="entry-title">', '</h1>' );
	} else {
		the_content();
	} ?>
</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		if ( is_singular() ) {
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'tempname' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		} else {
			echo '<div class="view-full-post"><a href="' . esc_url( get_permalink() ) . '" class="more-link button secondary" rel="bookmark">View Post</a></div>';
		}
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tempname' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php tempname_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
