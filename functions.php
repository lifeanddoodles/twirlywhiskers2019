<?php
/**
 * tempname functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tempname
 */

if ( ! function_exists( 'tempname_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tempname_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on tempname, use a find and replace
		 * to change 'tempname' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'tempname', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'tempname' ),
			'social' => __( 'Social Menu', 'tempname' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		* Enable support for Post Formats.
		* See https://developer.wordpress.org/themes/functionality/post-formats/
		*/
		function tempname_post_formats_setup() {
			add_theme_support( 'post-formats', array( 'aside', 'gallery', 'image', 'video', 'audio', 'link', 'quote', 'status' ) );
		 }
		 add_action( 'after_setup_theme', 'tempname_post_formats_setup' );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'tempname_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'tempname_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tempname_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'tempname_content_width', 640 );
}
add_action( 'after_setup_theme', 'tempname_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tempname_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tempname' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'tempname' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Hero Area', 'tempname' ),
		'description'   => __( 'Hero content', 'tempname' ),
		'id'            => 'hero-area',
		'before_widget' => '<div id="%1$s" class="widget hero %2$s"><div class="container">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h1 class="widget-title entry-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
	    'name'          => __( 'Footer 1', 'tempname' ),
	    'description'   => __( 'Footer Column 1.', 'tempname' ),
	    'id'            => 'footer-1',
	    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	    'after_widget'  => '</aside>',
	    'before_title'  => '<h3 class="widget-title">',
	    'after_title'   => '</h3>',
	) );
	register_sidebar( array(
	    'name'          => __( 'Footer 2', 'tempname' ),
	    'description'   => __( 'Footer Column 2.', 'tempname' ),
	    'id'            => 'footer-2',
	    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	    'after_widget'  => '</aside>',
	    'before_title'  => '<h3 class="widget-title">',
	    'after_title'   => '</h3>',
	) );
	register_sidebar( array(
	    'name'          => __( 'Footer 3', 'tempname' ),
	    'description'   => __( 'Footer Column 3.', 'tempname' ),
	    'id'            => 'footer-3',
	    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	    'after_widget'  => '</aside>',
	    'before_title'  => '<h3 class="widget-title">',
	    'after_title'   => '</h3>',
	) );
	register_sidebar( array(
	    'name'          => __( 'Footer 4', 'tempname' ),
	    'description'   => __( 'Footer Column 4.', 'tempname' ),
	    'id'            => 'footer-4',
	    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	    'after_widget'  => '</aside>',
	    'before_title'  => '<h3 class="widget-title">',
	    'after_title'   => '</h3>',
	) );
	register_sidebar( array(
	    'name'          => __( 'Legal', 'tempname' ),
	    'description'   => __( 'Legal links.', 'tempname' ),
	    'id'            => 'footer-5',
	    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	    'after_widget'  => '</aside>',
	    'before_title'  => '<h3 class="widget-title">',
	    'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'tempname_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tempname_scripts() {
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tempname_scripts' );

function tempname_assets() {
  wp_enqueue_style( 'tempname-stylesheet', get_template_directory_uri() . '/dist/css/style.css', array(), '1.0.0', 'all' );
  
	wp_enqueue_script( 'tempname-scripts', get_template_directory_uri() . '/dist/js/bundle.js', array(), '1.0.0', true );
	
	wp_enqueue_style( 'tempname-fonts' , 'https://fonts.googleapis.com/css?family=Muli:400,400i,700|Nunito+Sans:800' );

	// Add Font Awesome icons (http://fontawesome.io)
	wp_enqueue_script( 'tempname-fontawesome', 'https://use.fontawesome.com/4e2f6c9825.js', array(), '20190523', true );
}
add_action('wp_enqueue_scripts', 'tempname_assets');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Removing <p> and <br/> tags
 */
remove_filter( 'the_content', 'wpautop' );

/**
 * Word Count
 */
function word_count() {
	global $post;
    $content = strip_tags($post->post_content); 
	$content_words = str_word_count($content);
	return $content_words;
}

/**
 * Estimated Reading Time
 */
function estimate_reading_time() {
    $wpm = "200";
    $result = false;
    
    $content_words = word_count();

    $estimated_minutes = floor($content_words / $wpm);

    if ($estimated_minutes < 1) {
        $result = __("less than a minute", "tempname");
    }
    else if ($estimated_minutes > 60) {
        if ($estimated_minutes > 1440){
            $result = __("more than a day", "tempname");
        }
        else {
            $result = floor($estimated_minutes / 60) . " " . __("hours", "tempname");
        }
    }
    else if ($estimated_minutes == 1) {
        $result = $estimated_minutes . " " . __("minute", "tempname");
    }
    else {
        $result = $estimated_minutes . " " . __("minutes", "tempname");
    }
    return $result;
}

/**
 * Custom Excerpt
 */
/* Modify the read more link on the_excerpt() */
function et_excerpt_length($length) {
	return 20;
}
add_filter('excerpt_length', 'et_excerpt_length');

/* Add a link  to the end of our excerpt contained in a div for styling purposes and to break to a new line on the page.*/
function et_excerpt_more($more) {
global $post;
//echo word_count();

return sprintf( '...<br><div class="view-full-post"><a href="%1$s" class="more-link button">%2$s</a></div>',
	esc_url( get_permalink( get_the_ID() ) ),
	sprintf(
		/* translators: %s: Name of current post. */
		wp_kses( __( 'Read %s in '.estimate_reading_time(), 'tempname' ), array( 'span' => array( 'class' => array() ) ) ),
		the_title( '<span class="screen-reader-text">"', '"</span>', false )
	)
);
}
add_filter('excerpt_more', 'et_excerpt_more');

/**
 * Get URL from post
 */
function get_my_url() {
	if ( ! preg_match( '/<a\s[^>]*?href=[\'"](.+?)[\'"]/is', get_the_content(), $matches ) )
			return false;

	return esc_url_raw( $matches[1] );
}

/**
 * Add Meta Box
 */
function lesson_add_meta_boxes( $post ){
	//$post_types = array( 'post', 'lesson' );
	add_meta_box( 'lesson_meta_box', __( 'Lesson details', 'library_plugin' ), 'lesson_build_meta_box', array ('post','lesson'), 'side', 'low' );
}
add_action( 'add_meta_boxes', 'lesson_add_meta_boxes' );

// Build custom field meta box
function lesson_build_meta_box( $post ) {
	// make sure the form request comes from WordPress
	wp_nonce_field( basename( __FILE__ ), 'lesson_meta_box_nonce' );
// retrieve the _food_cholesterol current value
	$current_level = get_post_meta( $post->ID, '_lesson_level', true );
	// retrieve the _food_cholesterol current value
	$current_duration = get_post_meta( $post->ID, '_lesson_duration', true );
?>
	<!--<div class='inside'>-->

			<p><strong><?php _e( 'Level', 'library_plugin' ); ?></strong></p>
			<p>
					<input type="radio" name="level" value="Beginner" <?php checked( $current_level, 'Beginner' ); ?> /> Beginner<br />
					<input type="radio" name="level" value="Intermediate" <?php checked( $current_level, 'Intermediate' ); ?> /> Intermediate<br />
					<input type="radio" name="level" value="Advanced" <?php checked( $current_level, 'Advanced' ); ?> /> Advanced
			</p>

			<p><strong><?php _e( 'Duration', 'library_plugin' ); ?></strong></p>
			<p>
					<input type="text" name="duration" value="<?php echo $current_duration; ?>" /> 
			</p>
	<!--</div>-->

<?php
}

/**
* Store meta box data
*/
function lesson_save_meta_box_data( $post_id ) {
	// verify meta box nonce
	if ( !isset( $_POST['lesson_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['lesson_meta_box_nonce'], basename( __FILE__ ) ) ){
			return;
	}

	// return if autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
			return;
	}

	// Check the user's permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ){
			return;
	}

	// store custom fields value: level
	if ( isset( $_REQUEST['level'] ) ) {
			update_post_meta( $post_id, '_lesson_level', sanitize_text_field( $_POST['level'] ) );
	}

	// store custom fields value: duration
	if ( isset( $_REQUEST['duration'] ) ) {
			update_post_meta( $post_id, '_lesson_duration', sanitize_text_field( $_POST['duration'] ) );
	}
}
add_action( 'save_post', 'lesson_save_meta_box_data' );

/*
** Remove Extra Text on Titles
*/
function get_filtered_archive_title ( $title ) {

	if( is_category() || is_tax( ) ) {
			$title = single_cat_title( '', false );
	} elseif ( is_post_type_archive() ) {
			/* translators: Post type archive title. 1: Post type name */
			$title = post_type_archive_title( '', false );
	}

	return $title;

}

add_filter( 'get_the_archive_title', 'get_filtered_archive_title');