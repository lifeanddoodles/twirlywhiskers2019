
<?php
/**
 * The footer sidebar
 *
 */
if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' ) ) {
?>

<div id="supplementary">
	<div id="footer-widgets" class="footer-widgets widget-area clear" role="complementary">
		<?php
		if ( is_active_sidebar( 'footer-1' ) ) { ?><div class="footer-widget"><?php dynamic_sidebar( 'footer-1' ); ?></div> <?php }
		if ( is_active_sidebar( 'footer-2' ) ) { ?><div class="footer-widget"><?php dynamic_sidebar( 'footer-2' ); ?></div> <?php }
		if ( is_active_sidebar( 'footer-3' ) ) { ?><div class="footer-widget"><?php dynamic_sidebar( 'footer-3' ); ?></div> <?php }
		if ( is_active_sidebar( 'footer-4' ) ) { ?><div class="footer-widget"><?php dynamic_sidebar( 'footer-4' ); ?></div> <?php }
		?>
	</div><!-- #footer-sidebar -->
</div><!-- #supplementary -->

<?php
}
tempname_social_menu();
?>